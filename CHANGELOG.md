# Change Log

All notable changes to the "ruby-test-runner" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

## 0.3.9

Fix compatibility with Linux

## 0.3.8

Improve debugging capabilities and color output.

## 0.3.7

All commands are now prepended with `bundle exec`

## 0.3.6

Remove automatic change of the current working directory.

## 0.3.5

Slight refactoring and fixes for live test results - old test results are now properly deleted when a new test run is initialized

## 0.3.4

The extension will now automatically change the current working directory of the integrated terminal when accessing local gems in the `lib` directory

## 0.3.3

Errors in minitests are now properly registered an displayed in live test results

## 0.3.2

Failures outside of test definitions are now properly handled in live test results

## 0.3.1

Experimental feature - Live test result feed in the editor added

## 0.2.7

Running single tests now works properly in Rails 4

## 0.2.6

AppleScript used to run tests in iTerm replaced by a JXA (JavaScript for automation) script

## 0.2.5

It's now possible to run tests in iTerm instead of the integrated VSCode terminal.

Added new settings:
* `rubyTestRunner.openTestsInIterm`

## 0.2.4

Small naming fixes.

## 0.2.3

Added a new `Run current test file` button at the top of the editor.

## 0.2.2

A small fix to make the extension functional when no workspace is present.

## 0.2.1

The extension will now recognise the Rails version of your open project and use appropriate commands. Same goes for system tests, the extension will look for the `test/application_system_test_case.rb` file to see whether to use the `rails test:system` or the `rspec` command.

Added new settings:
* `rubyTestRunner.systemTestCommandWithoutRails`
* `rubyTestRunner.unitTestCommandRails4`

## 0.2.0

Added new settings:
* `rubyTestRunner.recognizedTestMethodNames`
* `rubyTestRunner.runTestCodeLensEnabled`
* `rubyTestRunner.runAllUnitTestsButtonEnabled`
* `rubyTestRunner.runAllSystemTestsButtonEnabled`
* `rubyTestRunner.runTestButtonEnabled`

## 0.1.4

Added CodeLens commands to run single tests within a test file with just a click.

## 0.1.3

Changed keybindings to use `Ctrl` instead of `Cmd` (buggy).

## 0.1.2

Added `Cmd + Shift + a` keybinding to run all unit tests.
Additional description of how to change keybindings in README.

## 0.1.1

Better README and icon added.

## 0.1.0

Initial release. It is possible to run Ruby tests with one click. There are three Status Bar Buttons:
* Unit tests - runs all unit tests
* System tests - runs all system tests
* Run test - runs the currently open test file. When there is a selection in the test file, the starting line of the selection will be passed to the test runner after a semicolon. (eg. `rails t /Users/some_user/some_test.rb:27`)

There is a key binding for running the currently open test file with `Shift + Cmd + T` or `Shift + Ctrl + T`