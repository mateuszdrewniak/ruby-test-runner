// VSCode API wrapper constant
const vscode = require('vscode')
const { RubyTestRunner } = require("./modules/ruby_test_runner.js")

/**
 * @param {vscode.ExtensionContext} context
 */

let rubyTestRunner

function activate(context) {
  rubyTestRunner = new RubyTestRunner(context)
  rubyTestRunner.activate()
}

// this method is called when your extension is deactivated
function deactivate() {}

module.exports = {
  activate,
  deactivate
}
